<?php 
include('../models/users.php');
$user = new User();
$user->insertUser($_POST);
?>
<html>
<head>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" />
<link rel="stylesheet" href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066" />
<link rel="stylesheet" href="./assets/css/toastr.css" />
<link rel="stylesheet" href="./assets/css/style.css" />
<script src="./assets/js/jquery-3.1.1.min.js"></script>
<!--<script src="./assets/js/custom.js"></script>-->
<script src="./assets/js/toastr.js"></script>
<script>
var domain = '<?php echo $subDomain;?>';//  + '.sirajulhaq.com';
</script>
</head>
<body>
	<div class="container">
    		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
								<h2>Account successfully created!</h2>
							</div>
						</div>
						<hr>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
