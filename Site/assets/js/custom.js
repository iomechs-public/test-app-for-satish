/*** Created by siraj ***/
$(document).ready(function(){
	$( "form" ).on( "submit", function( event ) {
		event.preventDefault();
		var data = $( this ).serialize()
		$.ajax({
			url: '/backend-api/users.php?action=create&domain='+domain+"&"+data,
			success: function(response){
				toastr.success("Account successfully created.", "Success");
			},
			error: function(error){
				toastr.error("Something went wrong, please try again later", "Error");
			}
		});
	});
});
