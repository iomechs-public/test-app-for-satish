<?php
/*** File created by Siraj ***/

$subDomain = $_SERVER['HTTP_HOST'];
?>
<html>
<head>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" />
<link rel="stylesheet" href="http://bootsnipp.com/dist/bootsnipp.min.css?ver=7d23ff901039aef6293954d33d23c066" />
<link rel="stylesheet" href="./assets/css/toastr.css" />
<link rel="stylesheet" href="./assets/css/style.css" />
<script src="./assets/js/jquery-3.1.1.min.js"></script>
<!--<script src="./assets/js/custom.js"></script>-->
<script src="./assets/js/toastr.js"></script>
<script>
var domain = '<?php echo $subDomain;?>';//  + '.sirajulhaq.com';
</script>
</head>
<body>
	<div class="container">
    		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
								<h2>Welcome <?php echo array_shift((explode(".",$_SERVER['HTTP_HOST'])));?></h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<a href="javascript:void(0)" class="active" id="login-form-link">Create Account</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="<?php echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . 'create-account.php'?>" method="post" style="display: block;">
                                    <input type="hidden" name="domain" value="<?php echo $subDomain;?>" />
									<div class="form-group">
										<input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Username/Email" value="" required>
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Create Account">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
